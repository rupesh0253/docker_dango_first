from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Register(models.Model):
    name = models.CharField(max_length = 255,null = True)
    mobileNo = models.BigIntegerField(null = True)
    department = models.TextField(max_length = 255,null = True)